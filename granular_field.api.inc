<?php

/**
 * Grain Variables XOR Children
 *
 * The distinction between grain render arrays and plain render arrays is
 * important. The grain render array concept imposes tighter requirements. A
 * grain's variables are keyed in its type array provided by some
 * hook_granular_field_info(). A grain render array cannot contain any children,
 * but its variables may.
 *
 * A grain is fundamentally a layer of indirection between a view mode's flat
 * render array and the beginning of the rendering pipeline. Each grain's
 * variables are structured arbitrarily with respect to each other based on the
 * grain author's prescription, regardless of how those variables are arranged
 * spatially in the render array. Arranging child render arrays with respect to
 * such siblings is infeasible without an ugly conceptual model.
 */

/**
 * Provide metadata describing a module's implementation of the Granular Field
 * API.
 *
 * @return Returns an array whose keys are the provided granular field type and
 * whose values are arrays containing key-value pairs:
 * - Parameters: If the grain uses optional or required parameters, then there
 * will be a 'parameters' key for an array of parameters and their default
 * values. Optional parameters are prefixed with '?'.
 * - Variables: Each variable name is provided as a key for an array of the
 * following key value pairs:
 *   - 'types' (optional) - An array of white-listed granular field types and
 *   field types that the variable can take. The generic strings
 *   'GRANULAR_FIELD_TYPES' and 'FIELD_TYPES' act as placeholders for all of
 *   their respective types.
 *   - '!types' (optional) - An array of black-listed granular field types and
 *   field types that the variable can take. The white-list placeholder can be
 *   used here as well.
 *
 * @todo Figure out a reasonable character to prefix optional parameters with.
 * @todo Think harder about difficulties arising from field cardinalities. Lists
 * seem like a problem, but maybe just leave administrator with the
 * control--"oops, my list has only one element in it...that shouldn't be a
 * list."
 */
function hook_granular_field_info() {
  return array(
    'injector' => array(
      'list' => array(
        'types' => array('FIELD_TYPES'),
      ),
      'injectee' => array(
        '!types' => array(),
      ),
      'parameters' => array(
        'position' => 0,
      ),
    ),
  );
}
