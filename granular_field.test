<?php

/**
 * Wrapped image use case.Paragraph
 *
 * The wrapped-image grain's render array will not contain any children, but 
 * variables instead. Tentatively, '#paragraph' and '#float' make the use case
 * readable, but will probably migrate to generic terms with namespace qualifiers
 * later.
 *
 * Using variables instead of children removes the resultant constraint: the two
 * render fragments do not necessarily appear as HTML siblings. The
 * <a href="http://drupal.org/node/988540">Field Injector</a> module provides
 * this functionality and uses the following approach:
 * <ol><li>Text is rendered surrounded by paragraph tags.</li>
 * <li>The resultant is exploded by '</p>'.</li>
 * <li>The parametrized paragraph number has its '<p>' tag replaced with '<p>'
 * followed by the rendered injectee.</li>
 * <li>The array is imploded and touched up.</li></ol>
 * The granular field framework should support such a workflow, although I'd 
 * rather see variable cardinality paragraph fields, begging the question: Can a
 * single text area dispatch its input to a bunch of fields after exploding
 * around "\n\s*\n". I don't care to answer the question.
 *
 * In the contributors forums, someone said that
 * <a href="http://drupal.org/project/ds">Display Suite</a> could provide the
 * functionality, also, but it sounded like they were talking about a hack (that
 * is, position the #float field before the #paragraph field and add a float-left
 * or float-right class). I consider this a hack because it requires the
 * administrator to know that the use case is supported, know what CSS is,
 * understand how it works.
 */

/**
 * Captioned image use case.
 *
 * Most of the discussion above is relevant to this use case also. If only
 * captioning above or below, the UI's spatial relation could function, but what
 * about side-set captions? Variables are still a better fit: '#subject' and
 * '#caption'. Philosophical difference: Is the administrator imposing structure
 * or layout? Distinction of roles implies the former.
 */
