<?php

/**
 * Type System
 *
 * A grain's variables have type restrictions implemented by
 * hook_granular_field_info(), built from subsets of field types and grain types,
 * with the grain's implementor providing defaults. An new administrative group
 * can alter or remove any of the restrictions, however (they're suggestions, not
 * requirements).
 *
 * @see hook_granular_field_info()
 */

/**
 * Field Cardinality
 *
 * A list grain can handle fields of cardinality greater than one. For
 * generality's sake, a random piece of data should be insertable between the
 * fields of such a list. An injector grain, similar at least in UI to
 * <a href="http://drupal.org/node/988540">Field Injector</a>, provides such
 * functionality.
 *
 * The injector use case requires parameters in addition to its variables. The
 * hook_granular_field_info() implementation must then introduce any parameters
 * that a grain needs in addition to its rendering data. Each parameter provides
 * a default value, and some syntax should be introduced for optional parameters.
 *
 * @see hook_granular_field_info()
 * @todo Should variables other than parameters and grain variables exist? If
 * not, the module could flag outliers.
 * @todo Strengthen analogy between granular fields and fields by admitting
 * variations in grain cardinality.
 */
